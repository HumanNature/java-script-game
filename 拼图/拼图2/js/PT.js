window.onload = function () {

    //获取需要使用的标签
    var oprate = $$("oprate");
    var sel = oprate.children[0];
    var btn = oprate.children[1];
    var imgs = $$("imgs");
    var step = oprate.children[2];

    function $$(idName) {
        return document.getElementById(idName);
    }
    //  难度	  //随机拼图的数组位置 	 // 步数
    var leval, data, steps = 0,
        path;
    // 加载函数
    function load(level) {
        imgs.innerHTML = (data + "").replace(/(\d+)\D*/g, `<div><img src="${level}/${path}/$1.gif" index="$1"></div>`);
        if (data.length != 0)
            imgs.children[data.indexOf(Math.pow(leval, 2))].innerHTML = "";
    }
    /*
     * 给图片所在的父级元素设置宽高
     */
    function imgStyle(leval) {
        imgs.setAttribute("style", `width:${80*leval}px;height:${80*leval}px;display:block`)
    }
    /*
     * 随机数据
     */
    function randData(n) {
        var randDigits = [];
        var max = Math.pow(n, 2) - 1;
        while (randDigits.length < max) {
            var randDigit = Math.floor(Math.random() * max) + 1;
            if (randDigits.indexOf(randDigit) == -1) {
                randDigits.push(randDigit);
            }
        }
        randDigits.push(max + 1);
        return randDigits;

    }

    btn.onclick = function () {
        path = Math.floor(Math.random() * 3) + 1
        var that = this;
        if (that.innerHTML == "重玩") {
            step.innerHTML = 0;
            steps = 0;
        }
        that.innerHTML = "重玩"

        leval = parseInt(sel.value);
        console.log(leval)
        imgStyle(leval);
        console.log(imgStyle(leval));
        data = randData(leval);
        console.log(data)
        load(leval)
    }

    imgs.onclick = function (e) {
        var evt = e || window.event;
        var target = evt.srcElement || evt.target;
        var clickPos = data.indexOf(parseInt(target.getAttribute("index")));
        var nullPos = data.indexOf(Math.pow(leval, 2));
        /*
         *  空白格子的位置在最左边 上一个数字的时候 也能实现交换  错误
         *  空白格子的位置在最右边 下一个数字的时候 也能实现交换  错误
         */
        var codition = (nullPos % leval == 0 && clickPos % leval == leval - 1) || (nullPos % leval == leval - 1 && clickPos % leval == 0);
        /*
         * 交换满足条件
         */
        if (!codition) {
            if (clickPos == nullPos - leval || clickPos == nullPos - 1 || clickPos == nullPos + 1 || clickPos == nullPos + leval) {
                step.innerHTML = ++steps;
                data[nullPos] = data[clickPos];
                data[clickPos] = Math.pow(leval, 2);
                load(leval)
            }
        }
    }
}