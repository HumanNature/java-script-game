var canvas = document.getElementById("canvas")
var ctx = canvas.getContext("2d")
canvasStyle = window.getComputedStyle(canvas, null) //获取画布的CSS属性
var size = 4 //设置棋盘边长为4格
canvas.width = canvas.height = parseInt(canvasStyle["width"]) //画布的宽高一致，正方形
var blocksize = canvas.width / size //根据格子的数量计算出单个格子的边长
var blocks = []


function newgame() { //根据棋盘尺寸初始化
    blocks = []
    for (var i = 0; i < size * size; i++) { //首先将全部格子都设为true
        blocks.push(true)
    }
    for (var i = 0; i < size + 1; i++) { //随机几个格子设为false 
        blocks[parseInt(Math.random() * blocks.length)] = false
    }
    draw()
}

function draw() {  //绘制棋盘
    var count = 0 //用于计算胜利条件的计数器
    for (var i = 0; i < blocks.length; i++) { //遍历所有的格子
        block = blocks[i] 
        if (block) { //根据格子状态设置填充颜色
            ctx.fillStyle = "white"
            count++
        }
        else {
            ctx.fillStyle = "black"
        }
        ctx.beginPath() //绘制圆形的棋子
        ctx.arc((i % size)*blocksize+blocksize/2,parseInt(i/size)*blocksize+blocksize/2,blocksize/2,0,Math.PI*2,false)
        ctx.stroke()
        ctx.fill()
    }
    
    if (count <= 1) { //胜利条件为棋盘上的白色棋子小于等于2个的时候获胜
        setTimeout(function () { //延迟1秒等待棋盘绘制完成
            alert("YOU WIN!")
            newgame() //点击确认弹出框后重新开始游戏
        }, 1000);

    }
}

newgame()
canvas.addEventListener("click", function (evt) { //用于计算翻转的棋子
    var cRect = canvas.getBoundingClientRect() //获取画布的尺寸位置属性
    var canvasX = Math.round(evt.clientX - cRect.left) //根据画布的页边距计算鼠标在画布内的坐标位置
    var canvasY = Math.round(evt.clientY - cRect.top)
    var x = parseInt(canvasX / blocksize) //结合格子的尺寸计算当前点击的格子行列坐标
    var y = parseInt(canvasY / blocksize)
    var origin = y * size + x // 根据行列坐标计算当前棋子在数组中的索引
    blocks[origin] = !blocks[origin] //翻转当前棋子
    blocks[origin - size] = !blocks[origin - size] //翻转上下的棋子
    blocks[origin + size] = !blocks[origin + size]
    if (origin % size == size - 1) {  //判断棋子是否在棋盘左右的边缘，并进行翻转限制
        blocks[origin - 1] = !blocks[origin - 1]
    }
    else if (origin % size == 0) {
        blocks[origin + 1] = !blocks[origin + 1]
    }
    else {
        blocks[origin - 1] = !blocks[origin - 1]
        blocks[origin + 1] = !blocks[origin + 1]
    }
    blocks = blocks.slice(0, size * size) //清理多余的数组内容
    draw()
})